import { Container } from "pixi.js";
import { Shared } from "../shared/shared";
import { Resources } from '../content/resource-list';
import { BoardController } from './board.controller';

export class GameController {

    private _gameContainer: Container
    private _boardController: BoardController

    constructor(
        private _app: Container
    ) { }

    public init() {
        this._gameContainer = new Container()

        this._app.addChild(this._gameContainer)

        Shared.resourceController._loader.onLoad.once(() => {
            this.initBackground()
            this.initBoard()
        })
    }

    private initBackground() {
        var bgSprite = Shared.resourceController.getResource(Resources.Background.name)

        this._gameContainer.addChild(bgSprite)
    }

    private initBoard() {
        this._boardController = new BoardController(this._gameContainer)

        this._boardController.init()
        this._boardController.throwShape()
    }
}