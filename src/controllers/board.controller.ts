import { Container } from 'pixi.js';
import { ShapeModule } from '../modules/shape.module';
import { take } from 'rxjs/operators';
import { Helper } from '../shared/helpers';
export class BoardController {

    private _shapesContainer: Container
    private shapes: ShapeModule[] = []
    private _board: any[] = []

    constructor(
        private _app: Container
    ) { }

    init() {
        this._shapesContainer = new Container()

        this._shapesContainer.x = 5
        this._shapesContainer.y = 6

        this._app.addChild(this._shapesContainer)

        this.createBoard()
    }

    private createBoard() {
        //creating board matrix
        let boardWidth = 10
        let boardHeight = 20

        while (boardHeight--) {
            let arr = new Array(boardWidth).fill(0)

            this._board.push(arr)
        }
    }

    public throwShape() {
        let shape: ShapeModule = this.createShape()

        shape.collideEmitter$
            .pipe(
                take(1)
            )
            .subscribe((shape: ShapeModule) => {
                //check if game is not over
                //remove line if its filled
                //drop another shape

                if (shape.shapeDetails.position.y == 0) {
                    this.gameOver()
                }
                else {
                    this.checkRow(shape)
                }

                this.throwShape()
            })
    }

    private createShape(): ShapeModule {
        let shapeId = Helper.UUID()
        let shape = new ShapeModule(shapeId, this._shapesContainer, this._board)

        this.shapes.push(shape)

        return shape
    }

    private checkRow(lastDropedShape: ShapeModule) {
        let matrix = Helper.normilizeMatrix(lastDropedShape.shapeMatrix)

        matrix.forEach((row, y) => {
            let rowIndex = lastDropedShape.shapeDetails.position.y + y
            let boardRow: any[] = this._board[rowIndex]

            if (!boardRow) return

            let filledMatrixCount = boardRow.reduce(
                (prev: number, curr: number) => prev + curr
            )

            //check if full row is filled
            if (filledMatrixCount == boardRow.length)
                this.removeRow(rowIndex)
        })
    }

    private removeRow(rowIndex: number) {
        for (let index = 0; index < this.shapes.length; index++) {
            const shape = this.shapes[index];
            const matrix = Helper.normilizeMatrix(shape.shapeMatrix)

            let shapePosition = shape.shapeDetails.position
            let shapeHeight = shapePosition.y + matrix.length
            let isShapeInsideRemovedRowBounds = rowIndex >= shapePosition.y && rowIndex <= shapeHeight

            if (isShapeInsideRemovedRowBounds) {
                shape.destroy()

                //clearing board matrix array
                matrix.forEach((row: any[], y) => {
                    row.forEach((value, x) => {
                        let boarRow = this._board[shape.shapeDetails.position.y + y]

                        boarRow[shape.shapeDetails.position.x + x] = 0
                    });
                })

                this.shapes.splice(index, 1)
                index--;
            }
        }
    }

    private gameOver() {
        this.shapes.length = 0

        this._shapesContainer.removeChildren()

        this._board.forEach((row: any[]) => {
            row.fill(0)
        })
    }
}