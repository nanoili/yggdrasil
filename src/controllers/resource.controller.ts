import { loaders, Sprite } from "pixi.js";
import { Resources } from "../content/resource-list";

export class ResourceController {

    private loadConcurrency: number = 10;
    private baseUrl: string = 'assets';
    public _loader: loaders.Loader

    constructor() { this.init() }

    public init() {
        this._loader = new loaders.Loader(this.baseUrl, this.loadConcurrency);

        for (const name in Resources) {
            if (!Resources.hasOwnProperty(name)) return

            let resource = (<any>Resources)[name]

            this._loader.add(resource.name, resource.url)
        }

        this._loader.load()
    }

    public getResource(name: string): Sprite {
        let resource = this._loader.resources[name]

        return new Sprite(resource.texture)
    }
}