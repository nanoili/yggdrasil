import { Container, Sprite, ticker, Texture } from 'pixi.js';
import { Shared } from '../shared/shared';
import { Resources } from '../content/resource-list';
import { KeyCodes } from '../content/key-code.enum';
import { fromEvent, merge, Subject } from 'rxjs';
import { takeUntil, filter, tap, share } from 'rxjs/operators';
import { Helper } from '../shared/helpers';

export class ShapeModule {

    private _boxShapeImages: string[] = [
        Resources.Block_Blue.name, Resources.Block_Cyan.name, Resources.Block_Green.name,
        Resources.Block_Orange.name, Resources.Block_Purple.name, Resources.Block_Red.name,
        Resources.Block_Yellow.name,
    ]

    public shapeDetails = {
        size: 26,
        velocity: 5,
        position: {
            x: 4,
            y: 0
        }
    }

    private shapeContainer: Container
    public shapeMatrix: any[]
    public collideEmitter$: Subject<ShapeModule> = new Subject()

    constructor(
        public id: string,
        public container: Container,
        public board: any[]
    ) {
        //on creation shape will star falling
        this.dropShape()
    }

    private dropShape() {
        let randomNumber = Helper.random(0, this._boxShapeImages.length - 1)
        let shapeName: string = this._boxShapeImages[randomNumber]

        let shapeImage: Sprite = Shared.resourceController.getResource(shapeName)
        this.shapeMatrix = this.getRandomShape()
        this.shapeContainer = this.buildShape(this.shapeMatrix, shapeImage)

        this.container.addChild(this.shapeContainer)

        this.createUpdateLoop()
        this.bindEvents()
    }

    private getRandomShape() {
        let I = [
            [1, 0, 0, 0],
            [1, 0, 0, 0],
            [1, 0, 0, 0],
            [1, 0, 0, 0],
        ]

        let J = [
            [0, 1, 0],
            [0, 1, 0],
            [1, 1, 0],
        ]

        let L = [
            [1, 0, 0],
            [1, 0, 0],
            [1, 1, 0],
        ]

        let T = [
            [0, 1, 0],
            [1, 1, 1],
            [0, 0, 0]
        ]

        let S = [
            [0, 1, 1],
            [1, 1, 0],
            [0, 0, 0]
        ]

        let O = [
            [1, 1],
            [1, 1],
        ]

        let Z = [
            [1, 1, 0],
            [0, 1, 1],
            [0, 0, 0]
        ]

        let shapes = [
            I,
            J,
            L,
            T,
            S,
            O,
            Z,
        ]

        let randomNumber = Helper.random(0, shapes.length - 1)

        return shapes[randomNumber]
    }

    private buildShape(matrix: any[], shape: Sprite): Container {
        let shapeContainer = new Container()

        matrix.forEach((row: any[], y) => {
            row.forEach((value, x) => {
                if (value == 0) return

                let offsetX = this.shapeDetails.size * x
                let offsetY = this.shapeDetails.size * y

                let shapeTexture = shape.texture
                let boxShape = new Sprite(shapeTexture)

                boxShape.width = this.shapeDetails.size
                boxShape.height = this.shapeDetails.size
                boxShape.position.set(offsetX, offsetY)

                shapeContainer.addChild(boxShape)
            })
        })

        //position and pivot setup for future rotations
        shapeContainer.pivot.set(
            shapeContainer.width / 2,
            shapeContainer.height / 2
        )

        shapeContainer.position.set(
            (this.shapeDetails.position.x * this.shapeDetails.size) + shapeContainer.width / 2,
            (this.shapeDetails.position.y * this.shapeDetails.size) + (shapeContainer.height / 2)
        )

        return shapeContainer
    }

    private createUpdateLoop() {
        let dropCounter = 0
        let dropInterval = 100

        Shared.ticker()
            .pipe(
                takeUntil(this.collideEmitter$),
                tap((deltaTime: number) => {
                    let velocity = this.shapeDetails.velocity * deltaTime
                    dropCounter += velocity
                }),
                filter(() => dropCounter > dropInterval)
            )
            .subscribe((deltaTime: number) => {
                dropCounter = 0

                this.updateState()
            })
    }

    private updateState() {
        this.shapeDetails.position.y++;

        let collided = this.isCollision(this.board, this.shapeMatrix, this.shapeDetails.position)

        if (collided) {
            this.shapeDetails.position.y--

            this.syncBoard(this.board, this.shapeMatrix, this.shapeDetails.position)

            // eventEmmiter: -> emitting collision to matrix controller and stopping shape movement
            this.collideEmitter$.next(this)
            this.collideEmitter$.complete()
        }
        else
            this.shapeContainer.y += this.shapeDetails.size
    }

    private syncBoard(boardMatrix: any[], shapeMatrix: any[], shapePosition: any) {
        let matrix = Helper.normilizeMatrix(shapeMatrix)

        matrix.forEach((row: any[], y: number) => {
            row.forEach((value, x) => {
                if (value == 0) return

                let boardRow = boardMatrix[y + shapePosition.y]

                boardRow[x + shapePosition.x] = value
            });
        })
    }

    private isCollision(boardMatrix: any[], shapeMatrix: any[], shapePosition: any) {
        let matrix = Helper.normilizeMatrix(shapeMatrix)

        for (let y = 0; y < matrix.length; ++y) {
            const shapeRow: number[] = matrix[y];
            const boardRow = boardMatrix[y + shapePosition.y]

            let shapeRowSum = shapeRow.reduce((prev: number, curr: number) => prev + curr)
            let shapeRowIsNotEmpty = shapeRowSum > 0

            for (let x = 0; x < shapeRow.length; ++x) {
                let shapeRowsColumnIsNotEmpty = shapeRow[x] !== 0
                let boardRowsColumnIsNotEmpty = boardRow && boardRow[x + shapePosition.x] !== 0
                let boardRowNotExits = !boardRow
                let shapeRowIsOutsideOfBound = boardRowNotExits && shapeRowIsNotEmpty

                if (
                    (shapeRowsColumnIsNotEmpty && boardRowsColumnIsNotEmpty)
                    ||
                    shapeRowIsOutsideOfBound
                ) {
                    return true
                }
            }
        }

        return false
    }

    private bindEvents() {
        fromEvent(document, 'keydown')
            .pipe(
                takeUntil(this.collideEmitter$)
            )
            .subscribe(({ keyCode }: KeyboardEvent) => {
                switch (keyCode) {
                    case KeyCodes.LEFT_ARROW:
                        this.move(-1)
                        break;
                    case KeyCodes.RIGHT_ARROW:
                        this.move(1)
                        break;
                    case KeyCodes.UP_ARROW:
                        this.rotate(this.shapeMatrix, 1)
                        break;
                    case KeyCodes.DOWN_ARROW:
                        this.changeSpeed(true)
                        break;
                }
            })

        fromEvent(document, 'keyup')
            .pipe(
                takeUntil(this.collideEmitter$)
            )
            .subscribe(({ keyCode }: KeyboardEvent) => {
                if (keyCode == KeyCodes.DOWN_ARROW)
                    this.changeSpeed(false)
            })
    }

    private rotate(shapeMatrix: any[], dir: number) {
        //direction -> 1 || -1
        // //matrix rotation
        for (let y = 0; y < shapeMatrix.length; y++) {
            for (let x = 0; x < y; x++) {

                [
                    shapeMatrix[x][y],
                    shapeMatrix[y][x]
                ] = [
                        shapeMatrix[y][x],
                        shapeMatrix[x][y]
                    ]
            }
        }

        if (dir > 0)
            shapeMatrix.forEach((row: any[]) => row.reverse())
        else
            shapeMatrix.reverse()

        //update view
        this.updateShapeViewByDegree(this.shapeContainer, dir)

        //check collision
        let collided = this.isCollision(this.board, this.shapeMatrix, this.shapeDetails.position)

        //if collided rotate back
        if (collided)
            this.rotate(shapeMatrix, -dir)
    }

    private updateShapeViewByDegree(shape: Container, dir: number) {
        this.shapeContainer.rotation += Helper.degrees2Radian(90 * dir)

        //just chaning -> width = height && height = width for roation and then updating x,y

        let rotatedDegrees = Helper.radians2Degree(shape.rotation)
        let rotationDirection = rotatedDegrees == 90 || rotatedDegrees == 270;

        let containerW = rotationDirection ? shape.height : shape.width
        let containerH = rotationDirection ? shape.width : shape.height

        let newPosition = {
            x: (this.shapeDetails.size * this.shapeDetails.position.x) + containerW / 2,
            y: (this.shapeDetails.size * this.shapeDetails.position.y) + containerH / 2
        }

        shape.position.set(
            newPosition.x,
            newPosition.y
        )
    }

    private move(direction: number) {
        //direction -> 1 || -1
        this.shapeContainer.x += (this.shapeDetails.size * direction)
        this.shapeDetails.position.x += direction

        let collided = this.isCollision(this.board, this.shapeMatrix, this.shapeDetails.position)

        if (collided) {
            this.shapeContainer.x -= (this.shapeDetails.size * direction)
            this.shapeDetails.position.x -= direction
        }
    }

    private changeSpeed(increase: boolean) {
        if (increase)
            this.shapeDetails.velocity = 30
        else
            this.shapeDetails.velocity = 5
    }

    public destroy() {
        this.container.removeChild(this.shapeContainer)
    }
}