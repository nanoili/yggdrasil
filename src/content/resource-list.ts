export const Resources = {
    Background: {
        name: 'Background',
        url: '/img/matrix.png'
    },
    Block_Blue: {
        name: 'Block_Blue',
        url: '/img/shapes/block_blue.png'
    },
    Block_Cyan: {
        name: 'Block_Cyan',
        url: '/img/shapes/block_cyan.png'
    },
    Block_Green: {
        name: 'Block_Green',
        url: '/img/shapes/block_green.png'
    },
    Block_Orange: {
        name: 'Block_Orange',
        url: '/img/shapes/block_orange.png'
    },
    Block_Purple: {
        name: 'Block_Purple',
        url: '/img/shapes/block_purple.png'
    },
    Block_Red: {
        name: 'Block_Red',
        url: '/img/shapes/block_red.png'
    },
    Block_Yellow: {
        name: 'Block_Yellow',
        url: '/img/shapes/block_yellow.png'
    },
}