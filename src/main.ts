import { Application } from 'pixi.js';
import { GameController } from './controllers/game.controller';

window.onload = () => {
    let _main = new Main()

    _main.init();

    //testing
    // (<any>window)._Main = _main
}

class Main {
    private _app: Application;
    private _gameController: GameController

    constructor() { }

    init() {
        this.createApplication()
        this.initGame()
    }

    private createApplication() {
        this._app = new Application({
            width: 272,
            height: 534,
            backgroundColor: 0x000
        });

        document
            .querySelector('#game-container')
            .appendChild(this._app.view);
    }

    private initGame() {
        this._gameController = new GameController(this._app.stage)

        this._gameController.init()
    }
}

