//this IIFE will help me create private (functions || variables) without classes
export const Helper = (function () {

    //just 4 fun
    var iAmPrivate = true

    function UUID() {
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    function random(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function degrees2Radian(degrees: number) {
        var pi = Math.PI;

        return degrees * (pi / 180);
    }

    function radians2Degree(radians: number) {
        let degrees = Math.round(radians * 180 / Math.PI)

        let normilize = Math.abs(degrees % 360 | 0)

        return normilize;
    }

    function normilizeMatrix(matrix: any[][]): any[] {
        //this function removes empty (rows || columns) from matrix 
        // empty (rows || columns) are added for easy rotation inside getRandomShape function

        /*
        1) in this example we have to remove every rows ([0] index) to normilize matrix:
        [               
            [0,0,1]                         [0,1] 
            [0,0,1]     << transofrm to >>  [0,1] 
            [0,1,1]                         [1,1] 
        ]

        2) in this exaplme we have to remove every row where columns sum is 0:
        [               
            [0,0,0]                         
            [0,0,1]     << transofrm to >>  [0,0,1]
            [1,1,1]                         [1,1,1]
        ]
        */

        let normilizedRow = normilizeMatrixRows(matrix)
        let normilized = normilizeMatrixColumns(normilizedRow)

        //returning normilized (aka. not filled with empty (rows || columns)) shapeMatrix.
        return normilized
    }

    function normilizeMatrixRows(matrix: any[][]) {
        let normilizedMatrix = matrix
            .filter((row: number[]) => {//removing empty rows
                let shapeRowSum = row.reduce((prev: number, curr: number) => prev + curr)

                return shapeRowSum != 0
            })

        return normilizedMatrix
    }

    function normilizeMatrixColumns(matrix: any[][]) {
        //calulcating every columns sum if sum will be 0 then ->
        //its an empty line and we need to get rid of it
        let columnSums = matrix.reduce((prev, curr) => {
            return prev.map((row, y) =>
                row + (curr[y] || 0)
            )
        })

        //filtering empty columns
        let normilized = matrix.map((row: any[], y: number) =>
            row.filter((val: number, x: number) => {
                let isColumnEmpty = columnSums[x] != 0

                return isColumnEmpty
            })
        )

        return normilized
    }


    return {
        UUID,
        random,
        degrees2Radian,
        radians2Degree,
        normilizeMatrix
    }
})()
