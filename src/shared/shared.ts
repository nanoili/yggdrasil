import { ResourceController } from '../controllers/resource.controller';
import { ticker } from 'pixi.js';
import { Observable, Observer } from 'rxjs';

export const Shared = {
    resourceController: new ResourceController(),
    ticker(): Observable<number> {
        return Observable.create((observer: Observer<number>) => {
            let _ticker = new ticker.Ticker()

            _ticker.add((delta: number) => {
                observer.next(delta)
            })

            _ticker.start()

            return () => _ticker.destroy()
        })
    }
}

