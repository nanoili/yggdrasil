const path = require('path');

module.exports = {
    entry: './src/main.ts',
    mode: "development",
    watch: true,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'src/')
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'src/'),
        compress: true,
        port: 9000,
        host: 'localhost', //'0.0.0.0',
        open: true
    }
};